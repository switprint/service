<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', __('Name').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('surname', __('Surname').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('surname', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('post_id', __('Post Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('post_id', $posts, null ,['class' => 'form-control', 'placeholder' => 'select']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('category_id', __('Category Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('category_id', $category, null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('is_master', __('Is Master').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::hidden('is_master', 0) !!}
        {!! Form::checkbox('is_master') !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('masters.index') !!}" class="btn btn-default">{{ __('Cancel') }}</a>
    </div>
</div>

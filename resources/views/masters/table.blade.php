<table class="table table-responsive" id="masters-table">
    <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Surname') }}</th>
            <th>{{ __('Post') }}</th>
            <th>{{ __('Category') }}</th>
            <th>{{ __('Is Master') }}</th>
            <th colspan="3">{{ __('Action') }} </th>
        </tr>
    </thead>
    <tbody>
    @foreach($masters as $master)
        <tr>
            <td>{!! $master->name !!}</td>
            <td>{!! $master->surname !!}</td>
            <td>{!! $master->post->name !!}</td>
            <td>{!! $master->category !!}</td>
            <td>{!! $master->is_master !!}</td>
            <td>
                {!! Form::open(['route' => ['masters.destroy', $master->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('masters.show', [$master->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('masters.edit', [$master->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('". __('Are you sure?')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

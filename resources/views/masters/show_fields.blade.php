<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $master->id !!}
</div>

<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $master->name !!}
</div>

<div class="form-group">
    {!! Form::label('surname', __('Surname').':') !!}
    {!! $master->surname !!}
</div>

<div class="form-group">
    {!! Form::label('post_id', __('Post Id').':') !!}
    {!! $master->post->name !!}
</div>

<div class="form-group">
    {!! Form::label('category_id', __('Category Id').':') !!}
    {!! $master->category !!}
</div>

<div class="form-group">
    {!! Form::label('is_master', __('Is Master').':') !!}
    {!! $master->is_master !!}
</div>

@if(isset($master->deleted_at))
    <div class="form-group">
        {!! Form::label('deleted_at', __('Deleted At').':') !!}
        {!! $master->deleted_at !!}
    </div>
@endif
@if(isset($master->created_at))
    <div class="form-group">
        {!! Form::label('created_at', __('Created At').':') !!}
        {!! $master->created_at !!}
    </div>
@endif
@if(isset($master->updated_at))
    <div class="form-group">
        {!! Form::label('updated_at', __('Updated At').':') !!}
        {!! $master->updated_at !!}
    </div>
@endif


@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('Master') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($master, ['route' => ['masters.update', $master->id], 'method' => 'patch']) !!}
                        @include('masters.fields')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $schedule->id !!}
</div>

<div class="form-group">
    {!! Form::label('work_from', __('Work From').':') !!}
    {!! $schedule->work_from !!}
</div>

<div class="form-group">
    {!! Form::label('work_to', __('Work To').':') !!}
    {!! $schedule->work_to !!}
</div>

<div class="form-group">
    {!! Form::label('lunch_from', __('Lunch From').':') !!}
    {!! $schedule->lunch_from !!}
</div>

<div class="form-group">
    {!! Form::label('launch_to', __('Launch To').':') !!}
    {!! $schedule->launch_to !!}
</div>

<div class="form-group">
    {!! Form::label('launch', __('Launch').':') !!}
    {!! $schedule->launch !!}
</div>

<div class="form-group">
    {!! Form::label('date', __('Date').':') !!}
    {!! $schedule->date !!}
</div>

<div class="form-group">
    {!! Form::label('masters_id', __('Masters Id').':') !!}
    {!! $schedule->master->title !!}
</div>

<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At').':') !!}
    {!! $schedule->deleted_at !!}
</div>

<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $schedule->created_at !!}
</div>

<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $schedule->updated_at !!}
</div>

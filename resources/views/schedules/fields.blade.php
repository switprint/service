<div class="form-group col-sm-6">
    {!! Form::label('work_from', __('Work From').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::time('work_from', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-6">
    {!! Form::label('work_to', __('Work To').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::time('work_to', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('launch', __('Launch').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::hidden('launch', 0) !!}
        {!! Form::checkbox('launch', '1') !!}
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('lunch_from', __('Lunch From').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::time('lunch_from', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-6">
    {!! Form::label('launch_to', __('Launch To').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::time('launch_to', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('date', __('Date').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::date('date', $schedule->date, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('masters_id', __('Masters Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('masters_id', $masters, null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('schedules.index') !!}" class="btn btn-default">{!! __('Cancel') !!}</a>
    </div>
</div>

<table class="table table-responsive" id="schedules-table">
    <thead>
        <tr>
            <th>{{ __('Work From') }}</th>
            <th>{{ __('Work To') }}</th>
            <th>{{ __('Lunch From') }}</th>
            <th>{{ __('Launch To') }}</th>
            <th>{{ __('Launch') }}</th>
            <th>{{ __('Date') }}</th>
            <th>{{ __('Masters Id') }}</th>
            <th colspan="3">{!! __('Action') !!} </th>
        </tr>
    </thead>
    <tbody>
    @foreach($schedules as $schedule)
        <tr>
            <td>{!! $schedule->work_from !!}</td>
            <td>{!! $schedule->work_to !!}</td>
            <td>{!! $schedule->lunch_from !!}</td>
            <td>{!! $schedule->launch_to !!}</td>
            <td>{!! $schedule->launch !!}</td>
            <td>{!! $schedule->date !!}</td>
            <td>{!! $schedule->master->title !!}</td>
            <td>
                {!! Form::open(['route' => ['schedules.destroy', $schedule->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('schedules.show', [$schedule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('schedules.edit', [$schedule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('". __('Are you sure?')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

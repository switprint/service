@if($post->id)
<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $post->id !!}
</div>
@endif


@if($post->name)
<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $post->name !!}
</div>
@endif


@if($post->created_at)
<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $post->created_at !!}
</div>
@endif


@if($post->updated_at)
<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $post->updated_at !!}
</div>
@endif


@if($post->deleted_at)
<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At').':') !!}
    {!! $post->deleted_at !!}
</div>
@endif



<li class="{{ Request::is('masters*') ? 'active' : '' }}">
    <a href="{!! route('masters.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Masters') }}</span></a>
</li>

<li class="{{ Request::is('journals*') ? 'active' : '' }}">
    <a href="{!! route('journals.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Journals') }}</span></a>
</li>

<li class="{{ Request::is('schedules*') ? 'active' : '' }}">
    <a href="{!! route('schedules.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Schedules') }}</span></a>
</li>

<li class="{{ Request::is('posts*') ? 'active' : '' }}">
    <a href="{!! route('posts.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Posts') }}</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Categories') }}</span></a>
</li>

<li class="{{ Request::is('masterServices*') ? 'active' : '' }}">
    <a href="{!! route('masterServices.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Master Services') }}</span></a>
</li>

<li class="{{ Request::is('services*') ? 'active' : '' }}">
    <a href="{!! route('services.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Services') }}</span></a>
</li>

<li class="{{ Request::is('clients*') ? 'active' : '' }}">
    <a href="{!! route('clients.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Clients') }}</span></a>
</li>

<li class="{{ Request::is('resources*') ? 'active' : '' }}">
    <a href="{!! route('resources.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Resources') }}</span></a>
</li>
<li class="{{ Request::is('companies*') ? 'active' : '' }}">
    <a href="{!! route('companies.index') !!}"><i class="fa fa-edit"></i><span>{{ __('Companies') }}</span></a>
</li>


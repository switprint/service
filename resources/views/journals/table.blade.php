<table class="table table-responsive" id="journals-table">
    <thead>
        <tr>
            <th>{{ __('Datetime') }}</th>

        <th>{{ __('Remind') }}</th>

        <th>{{ __('Comment') }}</th>

        <th>{{ __('Client Status') }}</th>

        <th>{{ __('Use Break') }}</th>

        <th>{{ __('Break Duration') }}</th>

        <th>{{ __('Is Accept') }}</th>

        <th>{{ __('Masters Id') }}</th>

        <th>{{ __('Clients Id') }}</th>

            <th colspan="3">{{ __('Action') }} </th>
        </tr>
    </thead>
    <tbody>
    @foreach($journals as $journal)
        <tr>
            <td>{!! $journal->datetime !!}</td>
            <td>{!! $journal->remind !!}</td>
            <td>{!! $journal->comment !!}</td>
            <td>{!! $journal->client_status !!}</td>
            <td>{!! $journal->use_break !!}</td>
            <td>{!! $journal->break_duration !!}</td>
            <td>{!! $journal->is_accept !!}</td>
            <td>{!! $journal->masters_id !!}</td>
            <td>{!! $journal->clients_id !!}</td>
            <td>
                {!! Form::open(['route' => ['journals.destroy', $journal->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('journals.show', [$journal->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('journals.edit', [$journal->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('". __('Are you sure?')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

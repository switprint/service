@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('Journal') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($journal, ['route' => ['journals.update', $journal->id], 'method' => 'patch']) !!}
                        @include('journals.fields')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

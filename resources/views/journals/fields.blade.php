<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('datetime', __('Datetime').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::input('dateTime-local', 'datetime', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('remind', __('Remind').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::number('remind', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comment', __('Comment').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('client_status', __('Client Status').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::hidden('client_status', 0) !!}
        {!! Form::checkbox('client_status', '1') !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('use_break', __('Use Break').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::hidden('use_break', 0) !!}
        {!! Form::checkbox('use_break', '1') !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('break_duration', __('Break Duration').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::number('break_duration', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('is_accept', __('Is Accept').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::hidden('is_accept', 0) !!}
        {!! Form::checkbox('is_accept', '1') !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('masters_id', __('Masters Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('masters_id', $masters, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('clients_id', __('Clients Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('clients_id', $clients, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('journals.index') !!}" class="btn btn-default">{{ __('Cancel') }}</a>
    </div>
</div>

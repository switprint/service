<div class="form-group">
    {!! Form::label('id', __('Id:')) !!}
    {!! $journal->id !!}
</div>


<div class="form-group">
    {!! Form::label('datetime', __('Datetime:')) !!}
    {!! $journal->datetime !!}
</div>


<div class="form-group">
    {!! Form::label('remind', __('Remind:')) !!}
    {!! $journal->remind !!}
</div>


<div class="form-group">
    {!! Form::label('comment', __('Comment:')) !!}
    {!! $journal->comment !!}
</div>


<div class="form-group">
    {!! Form::label('client_status', __('Client Status:')) !!}
    {!! $journal->client_status !!}
</div>


<div class="form-group">
    {!! Form::label('use_break', __('Use Break:')) !!}
    {!! $journal->use_break !!}
</div>


<div class="form-group">
    {!! Form::label('break_duration', __('Break Duration:')) !!}
    {!! $journal->break_duration !!}
</div>


<div class="form-group">
    {!! Form::label('is_accept', __('Is Accept:')) !!}
    {!! $journal->is_accept !!}
</div>


<div class="form-group">
    {!! Form::label('masters_id', __('Masters Id:')) !!}
    {!! $journal->masters_id !!}
</div>


<div class="form-group">
    {!! Form::label('clients_id', __('Clients Id:')) !!}
    {!! $journal->clients_id !!}
</div>


<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At:')) !!}
    {!! $journal->deleted_at !!}
</div>


<div class="form-group">
    {!! Form::label('created_at', __('Created At:')) !!}
    {!! $journal->created_at !!}
</div>


<div class="form-group">
    {!! Form::label('updated_at', __('Updated At:')) !!}
    {!! $journal->updated_at !!}
</div>



<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', __('Name').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::textarea('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('categories.index') !!}" class="btn btn-default">{{ __('Cancel') }}</a>
    </div>
</div>

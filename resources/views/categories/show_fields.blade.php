@if($category->id)
<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $category->id !!}
</div>
@endif


@if($category->name)
<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $category->name !!}
</div>
@endif


@if($category->deleted_at)
<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At').':') !!}
    {!! $category->deleted_at !!}
</div>
@endif


@if($category->created_at)
<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $category->created_at !!}
</div>
@endif


@if($category->updated_at)
<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $category->updated_at !!}
</div>
@endif



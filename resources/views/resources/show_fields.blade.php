@if($resource->id)
<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $resource->id !!}
</div>
@endif

@if($resource->name)
<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $resource->name !!}
</div>
@endif

@if($resource->created_at)
<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $resource->created_at !!}
</div>
@endif

@if($resource->updated_at)
<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $resource->updated_at !!}
</div>
@endif



<table class="table table-responsive" id="clients-table">
    <thead>
        <tr>
            <th>{{ __('Phone') }}</th>

        <th>{{ __('Name') }}</th>

        <th>{{ __('Surname') }}</th>

        <th>{{ __('Email') }}</th>

        <th>{{ __('Sex') }}</th>

        <th>{{ __('Discount') }}</th>

        <th>{{ __('Comment') }}</th>

        <th>{{ __('Birthday') }}</th>

        <th>{{ __('Is Blacklist') }}</th>

            <th colspan="3">{{ __('Action') }} </th>
        </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
        <tr>
            <td>{!! $client->phone !!}</td>
            <td>{!! $client->name !!}</td>
            <td>{!! $client->surname !!}</td>
            <td>{!! $client->email !!}</td>
            <td>{!! $client->sex !!}</td>
            <td>{!! $client->discount !!}</td>
            <td>{!! $client->comment !!}</td>
            <td>{!! $client->birthday !!}</td>
            <td>{!! $client->is_blacklist !!}</td>
            <td>
                {!! Form::open(['route' => ['clients.destroy', $client->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('clients.show', [$client->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('clients.edit', [$client->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('". __('Are you sure?')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

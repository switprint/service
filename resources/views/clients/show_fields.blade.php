@if($client->id)
<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $client->id !!}
</div>
@endif


@if($client->phone)
<div class="form-group">
    {!! Form::label('phone', __('Phone').':') !!}
    {!! $client->phone !!}
</div>
@endif


@if($client->name)
<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $client->name !!}
</div>
@endif


@if($client->surname)
<div class="form-group">
    {!! Form::label('surname', __('Surname').':') !!}
    {!! $client->surname !!}
</div>
@endif


@if($client->email)
<div class="form-group">
    {!! Form::label('email', __('Email').':') !!}
    {!! $client->email !!}
</div>
@endif


@if($client->sex)
<div class="form-group">
    {!! Form::label('sex', __('Sex').':') !!}
    {!! $client->sex !!}
</div>
@endif


@if($client->discount)
<div class="form-group">
    {!! Form::label('discount', __('Discount').':') !!}
    {!! $client->discount !!}
</div>
@endif


@if($client->comment)
<div class="form-group">
    {!! Form::label('comment', __('Comment').':') !!}
    {!! $client->comment !!}
</div>
@endif


@if($client->birthday)
<div class="form-group">
    {!! Form::label('birthday', __('Birthday').':') !!}
    {!! $client->birthday !!}
</div>
@endif


@if($client->is_blacklist)
<div class="form-group">
    {!! Form::label('is_blacklist', __('Is Blacklist').':') !!}
    {!! $client->is_blacklist !!}
</div>
@endif


@if($client->deleted_at)
<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At').':') !!}
    {!! $client->deleted_at !!}
</div>
@endif


@if($client->created_at)
<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $client->created_at !!}
</div>
@endif


@if($client->updated_at)
<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $client->updated_at !!}
</div>
@endif



<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('has_nds', __('Has Nds').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::hidden('has_nds', 0) !!}
        {!! Form::checkbox('has_nds', '1') !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', __('Name').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('phone', __('Phone').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('inn', __('Inn').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('inn', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('kpp', __('Kpp').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('kpp', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('legal_address', __('Legal Address').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('legal_address', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('okpo', __('Okpo').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('okpo', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ogrn', __('Ogrn').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('ogrn', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ogrn_date', __('Ogrn Date').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('ogrn_date', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('email', __('Email').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bik', __('Bik').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('bik', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_name', __('Bank Name').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_name_short', __('Bank Name Short').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('bank_name_short', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bank_addr', __('Bank Addr').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('bank_addr', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('account', __('Account').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('account', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('okopf', __('Okopf').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('okopf', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('fax', __('Fax').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('fax', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('site', __('Site').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('site', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('work_time', __('Work Time').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('work_time', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('fact_address', __('Fact Address').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('fact_address', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('corr_account', __('Corr Account').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('corr_account', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('companies.index') !!}" class="btn btn-default">{{ __('Cancel') }}</a>
    </div>
</div>

<table class="table table-responsive" id="companies-table">
    <thead>
        <tr>
            <th>{{ __('Has Nds') }}</th>

        <th>{{ __('Name') }}</th>

        <th>{{ __('Phone') }}</th>

        <th>{{ __('Inn') }}</th>

        <th>{{ __('Kpp') }}</th>

        <th>{{ __('Legal Address') }}</th>

        <th>{{ __('Okpo') }}</th>

        <th>{{ __('Ogrn') }}</th>

        <th>{{ __('Ogrn Date') }}</th>

        <th>{{ __('Email') }}</th>

        <th>{{ __('Bik') }}</th>

        <th>{{ __('Bank Name') }}</th>

        <th>{{ __('Bank Name Short') }}</th>

        <th>{{ __('Bank Addr') }}</th>

        <th>{{ __('Account') }}</th>

        <th>{{ __('Okopf') }}</th>

        <th>{{ __('Fax') }}</th>

        <th>{{ __('Site') }}</th>

        <th>{{ __('Work Time') }}</th>

        <th>{{ __('Fact Address') }}</th>

        <th>{{ __('Corr Account') }}</th>

            <th colspan="3">{{ __('Action') }} </th>
        </tr>
    </thead>
    <tbody>
    @foreach($companies as $company)
        <tr>
            <td>{!! $company->has_nds !!}</td>
            <td>{!! $company->name !!}</td>
            <td>{!! $company->phone !!}</td>
            <td>{!! $company->inn !!}</td>
            <td>{!! $company->kpp !!}</td>
            <td>{!! $company->legal_address !!}</td>
            <td>{!! $company->okpo !!}</td>
            <td>{!! $company->ogrn !!}</td>
            <td>{!! $company->ogrn_date !!}</td>
            <td>{!! $company->email !!}</td>
            <td>{!! $company->bik !!}</td>
            <td>{!! $company->bank_name !!}</td>
            <td>{!! $company->bank_name_short !!}</td>
            <td>{!! $company->bank_addr !!}</td>
            <td>{!! $company->account !!}</td>
            <td>{!! $company->okopf !!}</td>
            <td>{!! $company->fax !!}</td>
            <td>{!! $company->site !!}</td>
            <td>{!! $company->work_time !!}</td>
            <td>{!! $company->fact_address !!}</td>
            <td>{!! $company->corr_account !!}</td>
            <td>
                {!! Form::open(['route' => ['companies.destroy', $company->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('companies.show', [$company->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('companies.edit', [$company->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('". __('Are you sure?')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

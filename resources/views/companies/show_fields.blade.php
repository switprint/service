@if($company->id)
<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $company->id !!}
</div>
@endif


@if($company->created_at)
<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $company->created_at !!}
</div>
@endif


@if($company->updated_at)
<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $company->updated_at !!}
</div>
@endif


@if($company->deleted_at)
<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At').':') !!}
    {!! $company->deleted_at !!}
</div>
@endif


@if($company->has_nds)
<div class="form-group">
    {!! Form::label('has_nds', __('Has Nds').':') !!}
    {!! $company->has_nds !!}
</div>
@endif


@if($company->name)
<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $company->name !!}
</div>
@endif


@if($company->phone)
<div class="form-group">
    {!! Form::label('phone', __('Phone').':') !!}
    {!! $company->phone !!}
</div>
@endif


@if($company->inn)
<div class="form-group">
    {!! Form::label('inn', __('Inn').':') !!}
    {!! $company->inn !!}
</div>
@endif


@if($company->kpp)
<div class="form-group">
    {!! Form::label('kpp', __('Kpp').':') !!}
    {!! $company->kpp !!}
</div>
@endif


@if($company->legal_address)
<div class="form-group">
    {!! Form::label('legal_address', __('Legal Address').':') !!}
    {!! $company->legal_address !!}
</div>
@endif


@if($company->okpo)
<div class="form-group">
    {!! Form::label('okpo', __('Okpo').':') !!}
    {!! $company->okpo !!}
</div>
@endif


@if($company->ogrn)
<div class="form-group">
    {!! Form::label('ogrn', __('Ogrn').':') !!}
    {!! $company->ogrn !!}
</div>
@endif


@if($company->ogrn_date)
<div class="form-group">
    {!! Form::label('ogrn_date', __('Ogrn Date').':') !!}
    {!! $company->ogrn_date !!}
</div>
@endif


@if($company->email)
<div class="form-group">
    {!! Form::label('email', __('Email').':') !!}
    {!! $company->email !!}
</div>
@endif


@if($company->bik)
<div class="form-group">
    {!! Form::label('bik', __('Bik').':') !!}
    {!! $company->bik !!}
</div>
@endif


@if($company->bank_name)
<div class="form-group">
    {!! Form::label('bank_name', __('Bank Name').':') !!}
    {!! $company->bank_name !!}
</div>
@endif


@if($company->bank_name_short)
<div class="form-group">
    {!! Form::label('bank_name_short', __('Bank Name Short').':') !!}
    {!! $company->bank_name_short !!}
</div>
@endif


@if($company->bank_addr)
<div class="form-group">
    {!! Form::label('bank_addr', __('Bank Addr').':') !!}
    {!! $company->bank_addr !!}
</div>
@endif


@if($company->account)
<div class="form-group">
    {!! Form::label('account', __('Account').':') !!}
    {!! $company->account !!}
</div>
@endif


@if($company->okopf)
<div class="form-group">
    {!! Form::label('okopf', __('Okopf').':') !!}
    {!! $company->okopf !!}
</div>
@endif


@if($company->fax)
<div class="form-group">
    {!! Form::label('fax', __('Fax').':') !!}
    {!! $company->fax !!}
</div>
@endif


@if($company->site)
<div class="form-group">
    {!! Form::label('site', __('Site').':') !!}
    {!! $company->site !!}
</div>
@endif


@if($company->work_time)
<div class="form-group">
    {!! Form::label('work_time', __('Work Time').':') !!}
    {!! $company->work_time !!}
</div>
@endif


@if($company->fact_address)
<div class="form-group">
    {!! Form::label('fact_address', __('Fact Address').':') !!}
    {!! $company->fact_address !!}
</div>
@endif


@if($company->corr_account)
<div class="form-group">
    {!! Form::label('corr_account', __('Corr Account').':') !!}
    {!! $company->corr_account !!}
</div>
@endif



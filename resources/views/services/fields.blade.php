<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('parent_id', __('Parent Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('parent_id', $parents,  null, ['class' => 'form-control', 'placeholder' => __('Please select')]) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', __('Name').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('time', __('Time').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::number('time', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('resources_id', __('Resources Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('resources_id', $resources,  null, ['class' => 'form-control', 'placeholder' => __('Select')]) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('services.index') !!}" class="btn btn-default">{{ __('Cancel') }}</a>
    </div>
</div>

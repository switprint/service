<div class="form-group">
    {!! Form::label('id', __('Id').':') !!}
    {!! $service->id !!}
</div>

<div class="form-group">
    {!! Form::label('name', __('Name').':') !!}
    {!! $service->name !!}
</div>

<div class="form-group">
    {!! Form::label('time', __('Time').':') !!}
    {!! $service->time !!}
</div>

<div class="form-group">
    {!! Form::label('resources_id', __('Resources Id').':') !!}
    {!! $service->resource->name !!}
</div>

<div class="form-group">
    {!! Form::label('created_at', __('Created At').':') !!}
    {!! $service->created_at !!}
</div>

<div class="form-group">
    {!! Form::label('updated_at', __('Updated At').':') !!}
    {!! $service->updated_at !!}
</div>



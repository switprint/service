<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('master_id', __('Master Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('master_id', $masters, null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('service_id', __('Service Id').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::select('service_id', $services, null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('percent', __('Percent').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::number('percent', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('money', __('Money').':', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-9 col-md-6 col-lg-6">
        {!! Form::number('money', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="col-sm-9 col-sm-offset-3">
    {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('masterServices.index') !!}" class="btn btn-default">{{ __('Cancel') }}</a>
    </div>
</div>

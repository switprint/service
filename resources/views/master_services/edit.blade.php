@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('Master Service') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($masterService, ['route' => ['masterServices.update', $masterService->id], 'method' => 'patch']) !!}
                        @include('master_services.fields')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

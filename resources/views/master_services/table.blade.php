<table class="table table-responsive" id="masterServices-table">
    <thead>
        <tr>
            <th>{{ __('Master Id') }}</th>

        <th>{{ __('Service Id') }}</th>

        <th>{{ __('Percent') }}</th>

        <th>{{ __('Money') }}</th>

            <th colspan="3">{{ __('Action') }} </th>
        </tr>
    </thead>
    <tbody>
    @foreach($masterServices as $masterService)
        <tr>
            <td>{!! $masterService->master_id !!}</td>
            <td>{!! $masterService->service_id !!}</td>
            <td>{!! $masterService->percent !!}</td>
            <td>{!! $masterService->money !!}</td>
            <td>
                {!! Form::open(['route' => ['masterServices.destroy', $masterService->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('masterServices.show', [$masterService->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('masterServices.edit', [$masterService->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('". __('Are you sure?')."')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

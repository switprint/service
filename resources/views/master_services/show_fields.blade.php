<div class="form-group">
    {!! Form::label('id', __('Id:')) !!}
    {!! $masterService->id !!}
</div>


<div class="form-group">
    {!! Form::label('master_id', __('Master Id:')) !!}
    {!! $masterService->master !!}
</div>


<div class="form-group">
    {!! Form::label('service_id', __('Service Id:')) !!}
    {!! $masterService->service !!}
</div>


<div class="form-group">
    {!! Form::label('percent', __('Percent:')) !!}
    {!! $masterService->percent !!}
</div>


<div class="form-group">
    {!! Form::label('money', __('Money:')) !!}
    {!! $masterService->money !!}
</div>


<div class="form-group">
    {!! Form::label('deleted_at', __('Deleted At:')) !!}
    {!! $masterService->deleted_at !!}
</div>


<div class="form-group">
    {!! Form::label('created_at', __('Created At:')) !!}
    {!! $masterService->created_at !!}
</div>


<div class="form-group">
    {!! Form::label('updated_at', __('Updated At:')) !!}
    {!! $masterService->updated_at !!}
</div>



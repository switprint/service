drop table if EXISTS masters CASCADE;
CREATE TABLE "masters" (
"id"  SERIAL ,
"name" TEXT NOT NULL DEFAULT 'NULL' ,
"surname" TEXT ,
"post_id" INTEGER NOT NULL ,
"category_id" INTEGER NOT NULL ,
"is_master" BOOLEAN NOT NULL DEFAULT 'false' ,
"online" BOOLEAN NOT NULL DEFAULT 'true' ,
"use_mintime" BOOLEAN NOT NULL DEFAULT 'false' ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS schedules CASCADE;
CREATE TABLE "schedules" (
"id"  SERIAL ,
"work_from" timetz,
"work_to" TIMETZ ,
"lunch_from" TIMETZ ,
"launch_to" TIMETZ ,
"launch" BOOLEAN NOT NULL DEFAULT 'false' ,
"date" DATE ,
"masters_id" INTEGER ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS posts CASCADE;
CREATE TABLE "posts" (
"id"  SERIAL ,
"name" TEXT ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS services CASCADE;
CREATE TABLE "services" (
"id"  SERIAL ,
"name" TEXT ,
"parent_id" int,
"_rgt" int,
"_lft" integer,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
"time" INTEGER ,
"resources_id" INTEGER ,
PRIMARY KEY ("id")
);

drop table if EXISTS journals CASCADE;
CREATE TABLE "journals" (
"id"  SERIAL ,
"datetime" timestamp WITH TIME ZONE NOT NULL,
"remind" INTEGER /* напомить клиенту за */,
"comment" TEXT ,
"client_status" BOOLEAN ,
"use_break" BOOLEAN NOT NULL DEFAULT 'false' ,
"break_duration" INTEGER ,
"is_accept" BOOLEAN ,
"masters_id" INTEGER ,
"clients_id" INTEGER ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);
COMMENT ON COLUMN "journals"."remind" IS 'напомить клиенту за';

drop table if EXISTS clients CASCADE;
CREATE TABLE "clients" (
"id"  SERIAL ,
"phone" TEXT ,
"name" TEXT ,
"surname" TEXT ,
"email" TEXT ,
"sex" BOOLEAN ,
"discount" INTEGER ,
"comment" TEXT ,
"birthday" DATE ,
"is_blacklist" BOOLEAN ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS master_service CASCADE;
CREATE TABLE "master_service" (
"id"  SERIAL ,
"master_id" INTEGER ,
"service_id" INTEGER ,
"percent" INTEGER ,
"money" INTEGER ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS categories CASCADE;
CREATE TABLE "categories" (
"id"  SERIAL ,
"name" TEXT NOT NULL,
"parent_id" int,
"_rgt" int,
"_lft" integer,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS journals_services CASCADE;
CREATE TABLE "journals_services" (
"id"  SERIAL ,
"services_id" INTEGER ,
"journals_id" INTEGER ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS products CASCADE;
CREATE TABLE "products" (
"id"  SERIAL ,
"name" TEXT NOT NULL,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

drop table if EXISTS resources CASCADE;
CREATE TABLE "resources" (
"id"  SERIAL ,
"name" TEXT NOT NULL ,
"deleted_at" timestamptz,
"created_at" timestamptz,
"updated_at" timestamptz,
PRIMARY KEY ("id")
);

ALTER TABLE "masters" ADD FOREIGN KEY ("post_id") REFERENCES "posts" ("id");
ALTER TABLE "masters" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
ALTER TABLE "schedules" ADD FOREIGN KEY ("masters_id") REFERENCES "masters" ("id");
ALTER TABLE "services" ADD FOREIGN KEY ("resources_id") REFERENCES "resources" ("id");
ALTER TABLE "journals" ADD FOREIGN KEY ("masters_id") REFERENCES "masters" ("id");
ALTER TABLE "journals" ADD FOREIGN KEY ("clients_id") REFERENCES "clients" ("id");
ALTER TABLE "master_service" ADD FOREIGN KEY ("master_id") REFERENCES "masters" ("id");
ALTER TABLE "master_service" ADD FOREIGN KEY ("service_id") REFERENCES "services" ("id");
ALTER TABLE "journals_services" ADD FOREIGN KEY ("services_id") REFERENCES "services" ("id");
ALTER TABLE "journals_services" ADD FOREIGN KEY ("journals_id") REFERENCES "journals" ("id");

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'has_nds' => $faker->word,
        'name' => $faker->word,
        'phone' => $faker->word,
        'inn' => $faker->word,
        'kpp' => $faker->word,
        'legal_address' => $faker->word,
        'okpo' => $faker->word,
        'ogrn' => $faker->word,
        'ogrn_date' => $faker->word,
        'email' => $faker->word,
        'bik' => $faker->word,
        'bank_name' => $faker->word,
        'bank_name_short' => $faker->word,
        'bank_addr' => $faker->word,
        'account' => $faker->word,
        'okopf' => $faker->word,
        'fax' => $faker->word,
        'site' => $faker->word,
        'work_time' => $faker->word,
        'fact_address' => $faker->word,
        'corr_account' => $faker->word
    ];
});

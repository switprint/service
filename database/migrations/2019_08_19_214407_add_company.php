<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('has_nds')->nullable();
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('inn')->nullable();
            $table->string('kpp')->nullable();
            $table->string('legal_address')->nullable();
            $table->string('okpo')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('ogrn_date')->nullable();
            $table->string('email')->nullable();
            $table->string('bik')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_name_short')->nullable();
            $table->string('bank_addr')->nullable();
            $table->string('account')->nullable();
            $table->string('okopf')->nullable();
            $table->string('fax')->nullable();
            $table->string('site')->nullable();
            $table->string('work_time')->nullable();
            $table->string('fact_address')->nullable();
            $table->string('corr_account')->nullable();
            //$table->uuid('guid')->default('(md5(((random())::text || (clock_timestamp())::text)))::uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

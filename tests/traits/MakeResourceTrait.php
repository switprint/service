<?php

use Faker\Factory as Faker;
use App\Models\Resource;
use App\Repositories\ResourceRepository;

trait MakeResourceTrait
{
    /**
     * Create fake instance of Resource and save it in database
     *
     * @param array $resourceFields
     * @return Resource
     */
    public function makeResource($resourceFields = [])
    {
        /** @var ResourceRepository $resourceRepo */
        $resourceRepo = App::make(ResourceRepository::class);
        $theme = $this->fakeResourceData($resourceFields);
        return $resourceRepo->create($theme);
    }

    /**
     * Get fake instance of Resource
     *
     * @param array $resourceFields
     * @return Resource
     */
    public function fakeResource($resourceFields = [])
    {
        return new Resource($this->fakeResourceData($resourceFields));
    }

    /**
     * Get fake data of Resource
     *
     * @param array $postFields
     * @return array
     */
    public function fakeResourceData($resourceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $resourceFields);
    }
}

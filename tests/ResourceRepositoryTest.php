<?php

use App\Models\Resource;
use App\Repositories\ResourceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ResourceRepositoryTest extends TestCase
{
    use MakeResourceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ResourceRepository
     */
    protected $resourceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->resourceRepo = App::make(ResourceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateResource()
    {
        $resource = $this->fakeResourceData();
        $createdResource = $this->resourceRepo->create($resource);
        $createdResource = $createdResource->toArray();
        $this->assertArrayHasKey('id', $createdResource);
        $this->assertNotNull($createdResource['id'], 'Created Resource must have id specified');
        $this->assertNotNull(Resource::find($createdResource['id']), 'Resource with given id must be in DB');
        $this->assertModelData($resource, $createdResource);
    }

    /**
     * @test read
     */
    public function testReadResource()
    {
        $resource = $this->makeResource();
        $dbResource = $this->resourceRepo->find($resource->id);
        $dbResource = $dbResource->toArray();
        $this->assertModelData($resource->toArray(), $dbResource);
    }

    /**
     * @test update
     */
    public function testUpdateResource()
    {
        $resource = $this->makeResource();
        $fakeResource = $this->fakeResourceData();
        $updatedResource = $this->resourceRepo->update($fakeResource, $resource->id);
        $this->assertModelData($fakeResource, $updatedResource->toArray());
        $dbResource = $this->resourceRepo->find($resource->id);
        $this->assertModelData($fakeResource, $dbResource->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteResource()
    {
        $resource = $this->makeResource();
        $resp = $this->resourceRepo->delete($resource->id);
        $this->assertTrue($resp);
        $this->assertNull(Resource::find($resource->id), 'Resource should not exist in DB');
    }
}

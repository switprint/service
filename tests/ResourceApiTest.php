<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ResourceApiTest extends TestCase
{
    use MakeResourceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateResource()
    {
        $resource = $this->fakeResourceData();
        $this->json('POST', '/api/v1/resources', $resource);

        $this->assertApiResponse($resource);
    }

    /**
     * @test
     */
    public function testReadResource()
    {
        $resource = $this->makeResource();
        $this->json('GET', '/api/v1/resources/'.$resource->id);

        $this->assertApiResponse($resource->toArray());
    }

    /**
     * @test
     */
    public function testUpdateResource()
    {
        $resource = $this->makeResource();
        $editedResource = $this->fakeResourceData();

        $this->json('PUT', '/api/v1/resources/'.$resource->id, $editedResource);

        $this->assertApiResponse($editedResource);
    }

    /**
     * @test
     */
    public function testDeleteResource()
    {
        $resource = $this->makeResource();
        $this->json('DELETE', '/api/v1/resources/'.$resource->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/resources/'.$resource->id);

        $this->assertResponseStatus(404);
    }
}

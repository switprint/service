<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Company
 * @package App\Models
 *
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property boolean has_nds
 * @property string name
 * @property string phone
 * @property string inn
 * @property string kpp
 * @property string legal_address
 * @property string okpo
 * @property string ogrn
 * @property string ogrn_date
 * @property string email
 * @property string bik
 * @property string bank_name
 * @property string bank_name_short
 * @property string bank_addr
 * @property string account
 * @property string okopf
 * @property string fax
 * @property string site
 * @property string work_time
 * @property string fact_address
 * @property string corr_account
 */

class Company extends Model
{
    use SoftDeletes;

    public $table = 'companies';

    protected $dateFormat = 'Y-m-d H:i:sP';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'has_nds',
        'name',
        'phone',
        'inn',
        'kpp',
        'legal_address',
        'okpo',
        'ogrn',
        'ogrn_date',
        'email',
        'bik',
        'bank_name',
        'bank_name_short',
        'bank_addr',
        'account',
        'okopf',
        'fax',
        'site',
        'work_time',
        'fact_address',
        'corr_account'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'has_nds' => 'boolean',
        'name' => 'string',
        'phone' => 'string',
        'inn' => 'string',
        'kpp' => 'string',
        'legal_address' => 'string',
        'okpo' => 'string',
        'ogrn' => 'string',
        'ogrn_date' => 'string',
        'email' => 'string',
        'bik' => 'string',
        'bank_name' => 'string',
        'bank_name_short' => 'string',
        'bank_addr' => 'string',
        'account' => 'string',
        'okopf' => 'string',
        'fax' => 'string',
        'site' => 'string',
        'work_time' => 'string',
        'fact_address' => 'string',
        'corr_account' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'name' => 'required'
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Master
 * @package App\Models
 *
 * @property Post post
 * @property Category category
 * @property \Illuminate\Database\Eloquent\Collection journalsServices
 * @property \Illuminate\Database\Eloquent\Collection MasterService
 * @property \Illuminate\Database\Eloquent\Collection Journal
 * @property string name
 * @property string surname
 * @property integer post_id
 * @property integer category_id
 * @property boolean is_master
 * @property boolean online
 * @property boolean use_mintime
 */
class Master extends Model
{
    use SoftDeletes;

    protected $dateFormat = 'Y-m-d H:i:sP';

    public $table = 'masters';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'surname',
        'post_id',
        'category_id',
        'is_master',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'surname' => 'string',
        'post_id' => 'integer',
        'category_id' => 'integer',
        'is_master' => 'boolean',
        'online' => 'boolean',
        'use_mintime' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getIsMasterAttribute($attribute)
    {
        return $attribute ? __('Yes') : __('No');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'masters_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterServices()
    {
        return $this->hasMany(MasterService::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function journals()
    {
        return $this->hasMany(Journal::class);
    }

    public function getTitleAttribute()
    {
        return $this->name. ' '.$this->surname;
    }
}

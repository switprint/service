<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Journal
 * @package App\Models
 * @version November 21, 2018, 10:47 pm PKT
 *
 * @property Master master
 * @property Client client
 * @property \Illuminate\Database\Eloquent\Collection JournalsService
 * @property \Illuminate\Database\Eloquent\Collection masters
 * @property \Illuminate\Database\Eloquent\Collection masterService
 * @property time datetime
 * @property integer remind
 * @property string comment
 * @property boolean client_status
 * @property boolean use_break
 * @property integer break_duration
 * @property boolean is_accept
 * @property integer masters_id
 * @property integer clients_id
 */
class Journal extends Model
{
    use SoftDeletes;

    public $table = 'journals';

    protected $dateFormat = 'Y-m-d H:i:sP';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'datetime',
        'remind',
        'comment',
        'client_status',
        'use_break',
        'break_duration',
        'is_accept',
        'masters_id',
        'clients_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'remind' => 'integer',
        'comment' => 'string',
        'client_status' => 'boolean',
        'use_break' => 'boolean',
        'break_duration' => 'integer',
        'is_accept' => 'boolean',
        'masters_id' => 'integer',
        'clients_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function master()
    {
        return $this->belongsTo(Master::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function service()
    {
        return $this->belongsToMany(Service::class);
    }
}

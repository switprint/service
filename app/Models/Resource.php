<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Resource
 * @package App\Models
 *
 * @property string name
 */

class Resource extends Model
{
    use SoftDeletes;

    public $table = 'resources';

    protected $dateFormat = 'Y-m-d H:i:sP';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function service()
    {
        return $this->hasMany(Service::class, 'resources_is');
    }

    
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Schedule
 * @package App\Models
 *
 * @property Master master
 * @property \Illuminate\Database\Eloquent\Collection journalsServices
 * @property \Illuminate\Database\Eloquent\Collection masterService
 * @property \Illuminate\Database\Eloquent\Collection masters
 * @property \Illuminate\Database\Eloquent\Collection journals
 * @property timetz work_from
 * @property timetz work_to
 * @property timetz lunch_from
 * @property timetz launch_to
 * @property boolean launch
 * @property datetimetz date
 * @property integer masters_id
 */
class Schedule extends Model
{
    use SoftDeletes;

    public $table = 'schedules';

    protected $dateFormat = 'Y-m-d H:i:sP';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'work_from',
        'work_to',
        'lunch_from',
        'launch_to',
        'launch',
        'date',
        'masters_id'
    ];

    private function formatTime($time)
    {
        return isset($time) ? Carbon::createFromFormat('H:i:sP', $time)->format('H:i') : null;
    }

    public function getWorkFromAttribute($time)
    {
        return $this->formatTime($time);
    }

    public function getWorkToAttribute($time)
    {
        return $this->formatTime($time);
    }

    public function getLaunchToAttribute($time)
    {
        return $this->formatTime($time);
    }

    public function getLaunchFromAttribute($time)
    {
        return $this->formatTime($time);
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'launch' => 'boolean',
        'date' => 'date',
        'masters_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function master()
    {
        return $this->belongsTo(Master::class, 'masters_id');
    }
}

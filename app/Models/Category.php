<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 *
 * @property \Illuminate\Database\Eloquent\Collection journalsServices
 * @property \Illuminate\Database\Eloquent\Collection Master
 * @property \Illuminate\Database\Eloquent\Collection masterService
 * @property \Illuminate\Database\Eloquent\Collection journals
 * @property string name
 */

class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';

    protected $dateFormat = 'Y-m-d H:i:sP';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masters()
    {
        return $this->hasMany(Master::class);
    }

    public function __toString()
    {
        return $this->name;
    }
}

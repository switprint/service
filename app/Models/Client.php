<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 * @package App\Models
 *
 * @property \Illuminate\Database\Eloquent\Collection masters
 * @property \Illuminate\Database\Eloquent\Collection masterService
 * @property \Illuminate\Database\Eloquent\Collection Journal
 * @property string phone
 * @property string name
 * @property string surname
 * @property string email
 * @property boolean sex
 * @property integer discount
 * @property string comment
 * @property date birthday
 * @property boolean is_blacklist
 */

class Client extends Model
{
    use SoftDeletes;

    public $table = 'clients';

    protected $dateFormat = 'Y-m-d H:i:sP';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'phone',
        'name',
        'surname',
        'email',
        'sex',
        'discount',
        'comment',
        'birthday',
        'is_blacklist'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'phone' => 'string',
        'name' => 'string',
        'surname' => 'string',
        'email' => 'string',
        'sex' => 'boolean',
        'discount' => 'integer',
        'comment' => 'string',
        'birthday' => 'date',
        'is_blacklist' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function journals()
    {
        return $this->hasMany(Journal::class);
    }
}

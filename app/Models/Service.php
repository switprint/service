<?php

namespace App\Models;

use App\Models\Journal;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;

/**
 * Class Service
 * @package App\Models
 * @version November 21, 2018, 10:47 pm PKT
 *
 * @property Resource resource
 * @property \Illuminate\Database\Eloquent\Collection JournalsService
 * @property \Illuminate\Database\Eloquent\Collection masters
 * @property \Illuminate\Database\Eloquent\Collection MasterService
 * @property \Illuminate\Database\Eloquent\Collection journals
 * @property string name
 * @property integer parent_id
 * @property integer _rgt
 * @property integer _lft
 * @property integer time
 * @property integer resources_id
 */
class Service extends Model
{
    use SoftDeletes,
        NodeTrait;

    public $table = 'services';

    protected $dateFormat = 'Y-m-d H:i:sP';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'time',
        'resources_id',
        'parent_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'time' => 'integer',
        'resources_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function resource()
    {
        return $this->belongsTo(Resource::class, 'resources_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function journal()
    {
        return $this->belongsToMany(Journal::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterServices()
    {
        return $this->hasMany(MasterService::class);
    }
}

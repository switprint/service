<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MasterService
 * @package App\Models
 * @version November 21, 2018, 10:49 pm PKT
 *
 * @property Master master
 * @property Service service
 * @property \Illuminate\Database\Eloquent\Collection journalsServices
 * @property \Illuminate\Database\Eloquent\Collection masters
 * @property \Illuminate\Database\Eloquent\Collection journals
 * @property integer master_id
 * @property integer service_id
 * @property integer percent
 * @property integer money
 */
class MasterService extends Model
{
    use SoftDeletes;

    public $table = 'master_service';

    protected $dateFormat = 'Y-m-d H:i:sP';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'master_id',
        'service_id',
        'percent',
        'money'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_id' => 'integer',
        'service_id' => 'integer',
        'percent' => 'integer',
        'money' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function master()
    {
        return $this->belongsTo(Master::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}

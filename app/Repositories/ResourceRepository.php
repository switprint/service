<?php

namespace App\Repositories;

use App\Models\Resource;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ResourceRepository
 * @package App\Repositories
 *
 * @method Resource findWithoutFail($id, $columns = ['*'])
 * @method Resource find($id, $columns = ['*'])
 * @method Resource first($columns = ['*'])
*/

class ResourceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Resource::class;
    }
}

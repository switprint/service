<?php

namespace App\Repositories;

use App\Models\Company;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CompanyRepository
 * @package App\Repositories
 *
 * @method Company findWithoutFail($id, $columns = ['*'])
 * @method Company find($id, $columns = ['*'])
 * @method Company first($columns = ['*'])
*/

class CompanyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'has_nds',
        'name',
        'phone',
        'inn',
        'kpp',
        'legal_address',
        'okpo',
        'ogrn',
        'ogrn_date',
        'email',
        'bik',
        'bank_name',
        'bank_name_short',
        'bank_addr',
        'account',
        'okopf',
        'fax',
        'site',
        'work_time',
        'fact_address',
        'corr_account'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Company::class;
    }
}

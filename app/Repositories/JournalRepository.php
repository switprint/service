<?php

namespace App\Repositories;

use App\Models\Journal;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JournalRepository
 * @package App\Repositories
 * @version November 21, 2018, 10:47 pm PKT
 *
 * @method Journal findWithoutFail($id, $columns = ['*'])
 * @method Journal find($id, $columns = ['*'])
 * @method Journal first($columns = ['*'])
*/
class JournalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'datetime',
        'remind',
        'comment',
        'client_status',
        'use_break',
        'break_duration',
        'is_accept',
        'masters_id',
        'clients_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Journal::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\MasterService;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MasterServiceRepository
 * @package App\Repositories
 * @version November 21, 2018, 10:49 pm PKT
 *
 * @method MasterService findWithoutFail($id, $columns = ['*'])
 * @method MasterService find($id, $columns = ['*'])
 * @method MasterService first($columns = ['*'])
*/
class MasterServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_id',
        'service_id',
        'percent',
        'money'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterService::class;
    }
}

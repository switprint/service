<?php

namespace App\Repositories;

use App\Models\Master;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MasterRepository
 * @package App\Repositories
 * @version November 21, 2018, 10:49 pm PKT
 *
 * @method Master findWithoutFail($id, $columns = ['*'])
 * @method Master find($id, $columns = ['*'])
 * @method Master first($columns = ['*'])
*/
class MasterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'surname',
        'post_id',
        'category_id',
        'is_master',
        'online',
        'use_mintime'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Master::class;
    }
}

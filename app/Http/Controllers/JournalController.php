<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJournalRequest;
use App\Http\Requests\UpdateJournalRequest;
use App\Models\Client;
use App\Models\Master;
use App\Repositories\JournalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class JournalController extends AppBaseController
{
    /** @var  JournalRepository */
    private $journalRepository;

    public function __construct(JournalRepository $journalRepo)
    {
        $this->journalRepository = $journalRepo;
    }

    /**
     * Display a listing of the Journal.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->journalRepository->pushCriteria(new RequestCriteria($request));
        $journals = $this->journalRepository->all();

        return view('journals.index')
            ->with('journals', $journals);
    }

    /**
     * Show the form for creating a new Journal.
     *
     * @return Response
     */
    public function create()
    {

        $masters = Master::all()->pluck('name', 'id');
        $clients = Client::all()->pluck('name', 'id');

        return view('journals.create')->with(compact('masters', 'clients'));
    }

    /**
     * Store a newly created Journal in storage.
     *
     * @param CreateJournalRequest $request
     *
     * @return Response
     */
    public function store(CreateJournalRequest $request)
    {
        $input = $request->all();

        $journal = $this->journalRepository->create($input);

        Flash::success('Journal saved successfully.');

        return redirect(route('journals.index'));
    }

    /**
     * Display the specified Journal.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $journal = $this->journalRepository->findWithoutFail($id);

        if (empty($journal)) {
            Flash::error('Journal not found');

            return redirect(route('journals.index'));
        }

        return view('journals.show')->with('journal', $journal);
    }

    /**
     * Show the form for editing the specified Journal.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $journal = $this->journalRepository->findWithoutFail($id);

        if (empty($journal)) {
            Flash::error('Journal not found');

            return redirect(route('journals.index'));
        }

        $masters = Master::all()->pluck('name', 'id');
        $clients = Client::all()->pluck('name', 'id');

        return view('journals.edit')->with(compact('journal', 'masters', 'clients'));
    }

    /**
     * Update the specified Journal in storage.
     *
     * @param  int              $id
     * @param UpdateJournalRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJournalRequest $request)
    {
        $journal = $this->journalRepository->findWithoutFail($id);

        if (empty($journal)) {
            Flash::error('Journal not found');

            return redirect(route('journals.index'));
        }

        $journal = $this->journalRepository->update($request->all(), $id);

        Flash::success('Journal updated successfully.');

        return redirect(route('journals.index'));
    }

    /**
     * Remove the specified Journal from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $journal = $this->journalRepository->findWithoutFail($id);

        if (empty($journal)) {
            Flash::error('Journal not found');

            return redirect(route('journals.index'));
        }

        $this->journalRepository->delete($id);

        Flash::success('Journal deleted successfully.');

        return redirect(route('journals.index'));
    }
}

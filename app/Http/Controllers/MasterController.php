<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMasterRequest;
use App\Http\Requests\UpdateMasterRequest;
use App\Models\Category;
use App\Models\Post;
use App\Repositories\MasterRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MasterController extends AppBaseController
{
    /** @var  MasterRepository */
    private $masterRepository;

    public function __construct(MasterRepository $masterRepo)
    {
        $this->masterRepository = $masterRepo;
    }

    /**
     * Display a listing of the Master.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->masterRepository->pushCriteria(new RequestCriteria($request));
        $masters = $this->masterRepository->all();

        return view('masters.index')
            ->with('masters', $masters);
    }

    /**
     * Show the form for creating a new Master.
     *
     * @return Response
     */
    public function create()
    {
        $posts = Post::all()->pluck('name', 'id');

        $category = Category::all()->pluck('name', 'id');

        return view('masters.create')->with(compact('posts', 'category'));
    }

    /**
     * Store a newly created Master in storage.
     *
     * @param CreateMasterRequest $request
     *
     * @return Response
     */
    public function store(CreateMasterRequest $request)
    {
        $input = $request->all();

        $master = $this->masterRepository->create($input);

        Flash::success('Master saved successfully.');

        return redirect(route('masters.index'));
    }

    /**
     * Display the specified Master.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $master = $this->masterRepository->findWithoutFail($id);

        if (empty($master)) {
            Flash::error('Master not found');

            return redirect(route('masters.index'));
        }

        return view('masters.show')->with('master', $master);
    }

    /**
     * Show the form for editing the specified Master.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $master = $this->masterRepository->findWithoutFail($id);

        if (empty($master)) {
            Flash::error('Master not found');

            return redirect(route('masters.index'));
        }

        $posts = Post::all()->pluck('name', 'id');

        $category = Category::all()->pluck('name', 'id');

        return view('masters.edit')->with(compact('master', 'posts', 'category'));
    }

    /**
     * Update the specified Master in storage.
     *
     * @param  int              $id
     * @param UpdateMasterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMasterRequest $request)
    {
        $master = $this->masterRepository->findWithoutFail($id);

        if (empty($master)) {
            Flash::error('Master not found');

            return redirect(route('masters.index'));
        }

        $master = $this->masterRepository->update($request->all(), $id);

        Flash::success('Master updated successfully.');

        return redirect(route('masters.index'));
    }

    /**
     * Remove the specified Master from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $master = $this->masterRepository->findWithoutFail($id);

        if (empty($master)) {
            Flash::error('Master not found');

            return redirect(route('masters.index'));
        }

        $this->masterRepository->delete($id);

        Flash::success('Master deleted successfully.');

        return redirect(route('masters.index'));
    }
}

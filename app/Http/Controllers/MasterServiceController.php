<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMasterServiceRequest;
use App\Http\Requests\UpdateMasterServiceRequest;
use App\Models\Master;
use App\Models\Service;
use App\Repositories\MasterServiceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MasterServiceController extends AppBaseController
{
    /** @var  MasterServiceRepository */
    private $masterServiceRepository;

    public function __construct(MasterServiceRepository $masterServiceRepo)
    {
        $this->masterServiceRepository = $masterServiceRepo;
    }

    /**
     * Display a listing of the MasterService.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->masterServiceRepository->pushCriteria(new RequestCriteria($request));
        $masterServices = $this->masterServiceRepository->all();

        return view('master_services.index')
            ->with('masterServices', $masterServices);
    }

    /**
     * Show the form for creating a new MasterService.
     *
     * @return Response
     */
    public function create()
    {
        $masters = Master::all()->pluck('name', 'id');
        $services = Service::all()->pluck('name', 'id');

        return view('master_services.create')->with(compact('masters', 'services'));
    }

    /**
     * Store a newly created MasterService in storage.
     *
     * @param CreateMasterServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateMasterServiceRequest $request)
    {
        $input = $request->all();

        $masterService = $this->masterServiceRepository->create($input);

        Flash::success('Master Service saved successfully.');

        return redirect(route('masterServices.index'));
    }

    /**
     * Display the specified MasterService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $masterService = $this->masterServiceRepository->findWithoutFail($id);

        if (empty($masterService)) {
            Flash::error('Master Service not found');

            return redirect(route('masterServices.index'));
        }

        return view('master_services.show')->with('masterService', $masterService);
    }

    /**
     * Show the form for editing the specified MasterService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $masterService = $this->masterServiceRepository->findWithoutFail($id);

        if (empty($masterService)) {
            Flash::error('Master Service not found');

            return redirect(route('masterServices.index'));
        }
        $masters = Master::all()->pluck('name', 'id');
        $services = Service::all()->pluck('name', 'id');
        return view('master_services.edit')->with(compact('masterService', 'masters', 'services'));
    }

    /**
     * Update the specified MasterService in storage.
     *
     * @param  int              $id
     * @param UpdateMasterServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMasterServiceRequest $request)
    {
        $masterService = $this->masterServiceRepository->findWithoutFail($id);

        if (empty($masterService)) {
            Flash::error('Master Service not found');

            return redirect(route('masterServices.index'));
        }

        $masterService = $this->masterServiceRepository->update($request->all(), $id);

        Flash::success('Master Service updated successfully.');

        return redirect(route('masterServices.index'));
    }

    /**
     * Remove the specified MasterService from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $masterService = $this->masterServiceRepository->findWithoutFail($id);

        if (empty($masterService)) {
            Flash::error('Master Service not found');

            return redirect(route('masterServices.index'));
        }

        $this->masterServiceRepository->delete($id);

        Flash::success('Master Service deleted successfully.');

        return redirect(route('masterServices.index'));
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('masters', 'MasterController');

Route::resource('journals', 'JournalController');

Route::resource('schedules', 'ScheduleController');

Route::resource('masters', 'MasterController');

Route::resource('posts', 'PostController');

Route::resource('categories', 'CategoryController');

Route::resource('masterServices', 'MasterServiceController');

Route::resource('services', 'ServiceController');

Route::resource('clients', 'ClientController');

Route::resource('resources', 'ResourceController');

Route::resource('companies', 'CompanyController');
